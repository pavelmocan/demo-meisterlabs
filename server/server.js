const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');

app.use(cors());
app.use(bodyParser.json({ type: 'application/json' }));

app.get('/', (req, res) => res.send('Hello World!'));

app.post('/', (req, res) => {
    // simulating slow response with a timeout
    setTimeout(() => {
        let persistedId = req.body.id;
        if (persistedId < 0) {
            persistedId = Math.floor(Math.random() * 1000);
        }

        res.json({
            id: persistedId,
            name: req.body.name || ''
        });
    }, 1500);
});

app.patch('/', (req, res) => {
    setTimeout(() => {
        res.json({
            id: req.body.id,
            name: req.body.name
        });
    }, 500);
});

app.listen(3000, () => console.log('Example app listening on port 3000!'));