import './style';
import { Component } from 'preact';
import Axios from 'axios';

export default class App extends Component {
	timeout = null;
	state = {
		isFetching: false,							// global flag for blocking UI
		fetchingIds: [],							// specific reference for blocking row
		persons: [{ id: 1, name: 'John' }],
		newPerson: {
			name: '',
			lastId: 0,
		}
	};

	addPerson = (e) => {
		e.preventDefault();
		let { isFetching, fetchingIds, persons, newPerson } = this.state;
		let newPersonObj = {
			id: this.state.newPerson.lastId - 1,
			name: this.state.newPerson.name
		};

		persons.push(newPersonObj);
		fetchingIds.push(newPersonObj.id);

		this.setState({
			isFetching: true,
			fetchingIds,
			persons,
			newPerson: {
				name: '',
				lastId: newPersonObj.id
			}
		});

		Axios.post('http://localhost:3000', newPersonObj)
			.then((res) => {
				let { isFetching, fetchingIds, persons, newPerson } = this.state;
				this.setState({
					isFetching: false,
					fetchingIds: fetchingIds.filter(id => id !== newPersonObj.id),
					persons: persons.map(p => {
						return p.id === newPersonObj.id ? res.data : p
					}),
					newPerson
				});
				console.log('Posted', res.data, fetchingIds);
			})
			.catch((err) => {
				console.error('Error posting', newPersonObj);
			});
	}

	setNewPersonName = (e) => {
		let { isFetching, fetchingIds, persons, newPerson } = this.state;
		newPerson.name = e.target.value;
		this.setState({
			isFetching,
			fetchingIds,
			persons,
			newPerson
		});
	}

	handleChange = (e) => {
		clearTimeout(this.timeout);
		// simple debounce change event
		this.timeout = setTimeout(() => {
			let person = {
				id: parseInt(e.target.getAttribute('data-id')),
				name: e.target.value,
			};

			this.setState(Object.assign({}, this.state, {
				isFetching: true,
				fetchingIds: this.state.fetchingIds.concat(person.id),
				persons: this.state.persons.map(p => {
					return p.id === person.id ? person : p
				}),
			}));

			Axios.patch('http://localhost:3000', person)
				.then((res) => {
					let { isFetching, fetchingIds, persons, newPerson } = this.state;
					this.setState({
						isFetching: false,
						fetchingIds: fetchingIds.filter(id => id !== person.id),
						persons: persons.map(p => {
							return p.id === res.data.id ? res.data : p
						}),
						newPerson
					});
					console.log('Patched', res.data, fetchingIds);
				})
				.catch((err) => {
					console.error('Error patching', person);
				});
		}, 500);
	}

	render() {
		const persons = this.state.persons.map((p, i) => {
			let disabled = this.state.fetchingIds.indexOf(p.id) !== -1;

			return <tr>
				<td>#{p.id}</td>
				<td>
					<input type="text" value={p.name} data-id={p.id}
						onInput={this.handleChange} disabled={disabled} />
				</td>
			</tr>
		});

		const isFetching = this.state.isFetching ? <p>Global Loading Handler&hellip;</p> : '';

		return (
			<section id="wrapper">
				<h1>Post-Patch</h1>

				<form class="add-person" onSubmit={this.addPerson}>
					<h2>Add Person</h2>
					<input type="text" id="new-person-name" name="new-person-name"
						value={this.state.newPerson.name}
						onInput={this.setNewPersonName} />
					<button type="submit">+</button>
				</form>

				{isFetching}

				<table>
					<thead>
						<tr><th>ID</th><th>Name</th></tr>
					</thead>
					<tbody>{persons}</tbody>
				</table>
			</section>
		);
	}
}
