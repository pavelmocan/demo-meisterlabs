# Start

You will need to run two commands, one for FE, second for BE.

`npm run server` # starts BE

`npm run start`  # starts FE

# Behaviour

Application allows user to add a new Person by clicking the button [+].
The user can be added with a given name or empty string.
A POST request is done for adding a new user.

Changing a user's name is done with a PATCH request.

Both POST and PATCH requests send the `uid` and `name` parameters to BE.

Strategies for not messing up requests:
 - block UI globally until requests have completed `state.isFetching` 
 - keep a stack of uid for each user when an operation is in progress, in order to block activity on a user level, while allowing other users to be updated `state.isFetchingIds`
  - create a local stack of requests waiting to be completed, and going thru them one by one (not implemented)
  - update the state from the library doing the requests to achieve the effect globally

# Notes

The approach I took here is to write everything directly in Preact.
In a non-trivial example however, the application should be independend and not coupled with the view layer, like in my example.

In addition to the architecture consideration, if we are to follow something like DDD (Domain Driven Design), we would also set Person as an entity, with Value-Objects for the properties.
